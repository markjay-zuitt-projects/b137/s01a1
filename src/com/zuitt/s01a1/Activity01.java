package com.zuitt.s01a1;

public class Activity01 {

    public static void main(String[] args){
        // Create a variable for the first name.
        String first_name;
        first_name = "Juan";
        // Create a variable for the last name.
        String last_name;
        last_name = "Dela Cruz";
        // Create a variable for the grade in English.
        double grade_in_english;
        grade_in_english = 75.5;
        // Create a variable for the grade in Mathematics.
        double grade_in_mathematics;
        grade_in_mathematics = 80.1;
        // Create a variable for the grade in Science.
        double grade_in_science;
        grade_in_science = 91.6;
        // Compute the average grade of the three subjects.
        double total_grade = (grade_in_english + grade_in_science + grade_in_mathematics) / 3;
        // Display a message that states the user’s complete name and the average grade of the 3 subjects.


        System.out.println("Name: " + first_name + last_name);
        System.out.println("Grade: " + total_grade);
    }
}
